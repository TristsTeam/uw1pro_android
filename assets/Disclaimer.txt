Disclaimer:
These workout tutorials provide instructional and educational advice and theory on areas of health and fitness and related training protocols.

UltimateFitnessApp.com & Ultimate Core Workout advises and urges you to consult with your physician or doctor / health specialist prior to engaging in any of the exercises, workouts or activities presented in this app or any other workout and/or fitness program or app.

The information found here and at any related websites is the author�s opinion only and is not presented by a medical practitioner and is for educational and informational purposes only. The content is not intended to be a substitute for professional medical advice, diagnosis, or treatment. Always seek the advice of your physician or other qualified health provider with any questions you may have regarding a medical condition. 
