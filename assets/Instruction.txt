Instructions:
To begin your training simply choose a workout.
*I suggest you then try out the 'Off The Clock' workout first to allow you to scroll through each of the exercises of your chosen workout and get familiar with the moves.

Then when you're comfortable you have two workout choices:

Total Time Workout
this workout has you working on the clock.  As soon as you press start the timer begins. You can scroll through the exercises as you complete them and do as many circuits as you plan to.  Once complete press 'Finish'.  You will be shown your final time and can open the notes tool to record your results and progress (you can also open previous notes to compare / edit etc)

Off The Clock Workout 
As the name says:  the clock is not on now, you can go at your own pace and begin from ANY exercise for your circuit flow.

So as you see the structure is really straightforward:
You have a series of exercises to finish in circuit fashion and can either have the timer - running in the background to time your full workout - or perform the workout at your pace with no timer timing you.

The Workout Info Screen:
This pre workout screen shows you the reps/sets/circuits etc you need to do for each workout. depending if you are a beginner/intermediate/advanced or pro.  You will have a set number of reps to perform and also a set number of circuits.  Press 'I' at any time to pull up this info screen for reference.

KEY:
Reps:  
One full start to finish movement of a particular exercise.

Circuit:  
The start to finish collection of chosen exercises (i.e. 7 exercises, 10 reps each exercise, making up 1 full circuit)

TOOLS:
UCW also has some useful tools to assist you in your workouts:

EQUIPMENT
UFA 'Recommended Fitness Equipment' - www.ultimatefitnessapp.com/fitness-equipment

Music Player:  
Listen to your favorite music playlists as you train.

Notepad:  
Record all your own notes inside the app so you can refer to them later and track your progress.

Email: 
Email your results to any of your friends or family.

Share Social:  
Tell everyone about your amazing workouts and let others you care about know about this app directly on Facebook by liking our Facebook Page and sharing with your friends.

Well, that should get you going and on the road to amazing results.

I really hope you enjoy the workouts, and please come on over to our website and become a VIP Insider for free where you can get more free apps and news of our upcoming apps as well as free workouts, news, tips, fitness information and much, much more.

www.UltimateFitnessApp.com
Good luck!
Cheers
Tristan
UFA Developer / CPT Personal Trainer
