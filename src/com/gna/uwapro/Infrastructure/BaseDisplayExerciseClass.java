package com.gna.uwapro.Infrastructure;

import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.gna.uwapro.R;

public class BaseDisplayExerciseClass extends CommonBaseClass {
	protected String[] exerciseTextArray;
	protected String[] exerciseNameArray;
	protected int[] firstPictureArray;
	protected int[] secondPictureArray;
	protected int[] thirdPictureArray;
	protected int[] imageCount;

	protected ImageView firstImage;
	protected ImageView secondImage;
	protected ImageButton rightArrowBtn;
	protected ImageButton leftArrowBtn;
	protected ImageButton cancelBtn;
	protected TextView exerciseText;
	protected TextView exerciseCount;
	protected TextView exerciseName;
	protected ImageView titleView;
	protected ImageButton infoBtn;

	public void intializeViews() {
		firstImage = (ImageView) findViewById(R.id.firstImage);
		secondImage = (ImageView) findViewById(R.id.secondImage);
		rightArrowBtn = (ImageButton) findViewById(R.id.rightArrowBtn);
		leftArrowBtn = (ImageButton) findViewById(R.id.leftArrowBtn);
		exerciseText = (TextView) findViewById(R.id.exerciseText);
		exerciseCount = (TextView) findViewById(R.id.exerciseCount);
		exerciseName = (TextView) findViewById(R.id.exerciseName);
		titleView = (ImageView) findViewById(R.id.titleView);
		cancelBtn = (ImageButton) findViewById(R.id.cancelBtn);
		infoBtn = (ImageButton) findViewById(R.id.infoBtn);

	}

	public void workoutAExercise() {
		titleView.setBackgroundResource(R.drawable.title01);
		exerciseTextArray = new String[7];
		exerciseNameArray = new String[7];
		firstPictureArray = new int[7];
		secondPictureArray = new int[7];
		thirdPictureArray = new int[7];
		imageCount = new int[7];

		exerciseTextArray[0] = "With feet on an elevated surface, palms shoulder width apart, perform a push up.Keep your core tight.";
		exerciseTextArray[1] = "Holding two dumbbells in a �hammer grip�, lunge forward on your left leg, as you curl both weights upward. Pause, control balance, return and repeat on the other side.";
		exerciseTextArray[2] = "Keeping two dumbbells at a 90 degree shoulder position, begin to perform a jumping jack as you press both weights upward. Continue this motion.  Do not lower weights below 90 degree arm position.  Tense your core.";
		exerciseTextArray[3] = "Standing, with two dumbbells, tense your core and begin pressing the weight upward, return then repeat the other side.";
		exerciseTextArray[4] = "Holding heavier Dumbbells, bend forward, keeping your back in a controlled braced position, squat/drop downward, then extend your body, straighten legs and thrust back upward, with heels digging into the floor. Pause and repeat.";
		exerciseTextArray[5] = "Laying on your back, hold two dumbbells up with arms straight, then moving only your forearms, lower weight in a hammer grip to the side of your head, pause then return to start. (for advanced, move elbows further toward head to increase continual tension on the tricep.";
		exerciseTextArray[6] = "Holding two dumbbells in a push up position, brace your core so you do not dip or arch your hips, begin to row alternately each dumbbell to your side, pausing then return and repeat.";

		exerciseNameArray[0] = "Elevated Push Up";
		exerciseNameArray[1] = "Lung Hammer Curl";
		exerciseNameArray[2] = "DB Jumping Jack";
		exerciseNameArray[3] = "Alt Shoulder Press";
		exerciseNameArray[4] = "DB Deadlift";
		exerciseNameArray[5] = "DB SkullKrusher [Floor]";
		exerciseNameArray[6] = "Renegade Rows";

		firstPictureArray[0] = R.drawable.w1_exercise01_01;
		firstPictureArray[1] = R.drawable.w1_exercise02_01;
		firstPictureArray[2] = R.drawable.w1_exercise03_01;
		firstPictureArray[3] = R.drawable.w1_exercise04_01;
		firstPictureArray[4] = R.drawable.w1_exercise05_01;
		firstPictureArray[5] = R.drawable.w1_exercise06_01;
		firstPictureArray[6] = R.drawable.w1_exercise07_01;

		secondPictureArray[0] = R.drawable.w1_exercise01_02;
		secondPictureArray[1] = R.drawable.w1_exercise02_02;
		secondPictureArray[2] = R.drawable.w1_exercise03_02;
		secondPictureArray[3] = R.drawable.w1_exercise04_02;
		secondPictureArray[4] = R.drawable.w1_exercise05_02;
		secondPictureArray[5] = R.drawable.w1_exercise06_02;
		secondPictureArray[6] = R.drawable.w1_exercise07_02;

		thirdPictureArray[0] = 0;
		thirdPictureArray[1] = 0;
		thirdPictureArray[2] = 0;
		thirdPictureArray[3] = 0;
		thirdPictureArray[4] = 0;
		thirdPictureArray[5] = 0;
		thirdPictureArray[6] = 0;

		imageCount[0] = 2;
		imageCount[1] = 2;
		imageCount[2] = 2;
		imageCount[3] = 2;
		imageCount[4] = 2;
		imageCount[5] = 2;
		imageCount[6] = 2;

	}

	public void workoutBExercise() {
		titleView.setBackgroundResource(R.drawable.title02);
		exerciseTextArray = new String[8];
		exerciseNameArray = new String[8];
		firstPictureArray = new int[8];
		secondPictureArray = new int[8];
		thirdPictureArray = new int[8];
		imageCount = new int[8];

		exerciseTextArray[0] = "Using a pull up machine or any sturdy pull up equipment, grip with a wide overhand grip and pull yourself up, tensing your core, so your chest nearly touches the bar.  Pause, return slowly and repeat.";
		exerciseTextArray[1] = "Using either piece of equipment, grip securely and with a solid stance, swing the weight between your legs, thrust hips forward slightly as you swing the weight upward to around chest or chin level, repeat allowing momentum to take the weight.";
		exerciseTextArray[2] = "Holing two dumbbells at your shoulders, elbows toward the front, squat downward so your thighs are parallel and then thrust upward, as you simultaneously press the DBs overhead.  Return under control and repeat.";
		exerciseTextArray[3] = "With the DB on the floor at your inside ankle, turn sideways and squat downward (do not bend at your wait � use your legs) grab the DB and then using your legs and tensing your core, thrust up twisting at the waist and push the DB to the opposite side as if placing it on a shelf.  Pause, return and repeat set amount of reps. Then do the other side.";
		exerciseTextArray[4] = "Holding your DB�s with palms facing forward, slight bend in the knee to alleviate back tension, curl the weights to your shoulders then return slower to start position.  Repeat.";
		exerciseTextArray[5] = "Holding your weights in a secure grip, bend at your waist, but also bend your knees slightly so your back is flat and your core tight to stop your back arching, row the weights to your waist line, pause and repeat.";
		exerciseTextArray[6] = "Holding a DB with underhand grip in left had, lean forward raising the DB as your left leg goes Packard, pause trying to get as flat as possible with core tensed.  Repeat other side.";
		exerciseTextArray[7] = "In a push up stance, tense your core. Bring your left leg up and outward, bending it.  Pause and then repeat the other side.  You can do an optional push up between each knee out.";

		exerciseNameArray[0] = "Pull Ups";
		exerciseNameArray[1] = "KettleBell or DB Swing";
		exerciseNameArray[2] = "Thrusters";
		exerciseNameArray[3] = "DB Put Ups";
		exerciseNameArray[4] = "DB Curl";
		exerciseNameArray[5] = "DB Bent Double Row";
		exerciseNameArray[6] = "DB Lean";
		exerciseNameArray[7] = "Knee Outs";

		firstPictureArray[0] = R.drawable.w2_exercise01_01;
		firstPictureArray[1] = R.drawable.w2_exercise02_01;
		firstPictureArray[2] = R.drawable.w2_exercise03_01;
		firstPictureArray[3] = R.drawable.w2_exercise04_01;
		firstPictureArray[4] = R.drawable.w2_exercise05_01;
		firstPictureArray[5] = R.drawable.w2_exercise06_01;
		firstPictureArray[6] = R.drawable.w2_exercise07_01;
		firstPictureArray[7] = R.drawable.w2_exercise08_01;

		secondPictureArray[0] = R.drawable.w2_exercise01_02;
		secondPictureArray[1] = R.drawable.w2_exercise02_02;
		secondPictureArray[2] = R.drawable.w2_exercise03_02;
		secondPictureArray[3] = R.drawable.w2_exercise04_02;
		secondPictureArray[4] = R.drawable.w2_exercise05_02;
		secondPictureArray[5] = R.drawable.w2_exercise06_02;
		secondPictureArray[6] = R.drawable.w2_exercise07_02;
		secondPictureArray[7] = R.drawable.w2_exercise08_02;

		thirdPictureArray[0] = 0;
		thirdPictureArray[1] = R.drawable.w2_exercise02_03;
		thirdPictureArray[2] = 0;
		thirdPictureArray[3] = 0;
		thirdPictureArray[4] = 0;
		thirdPictureArray[5] = 0;
		thirdPictureArray[6] = 0;
		thirdPictureArray[7] = 0;

		imageCount[0] = 2;
		imageCount[1] = 3;
		imageCount[2] = 2;
		imageCount[3] = 2;
		imageCount[4] = 2;
		imageCount[5] = 2;
		imageCount[6] = 2;
		imageCount[7] = 2;

	}

	public void workoutCExercise() {
		titleView.setBackgroundResource(R.drawable.title03);
		exerciseTextArray = new String[8];
		exerciseNameArray = new String[8];
		firstPictureArray = new int[8];
		secondPictureArray = new int[8];
		thirdPictureArray = new int[8];
		imageCount = new int[8];

		exerciseTextArray[0] = "With palms flat and under shoulders, and elbows tight to your side, perform the CGPU.  Go on your knees if needed.";
		exerciseTextArray[1] = "With feet shoulder width apart perform a body weight squat, get your thighs parallel then explode upward, leaving the floor slightly then landing softly back into the squat�repeat.";
		exerciseTextArray[2] = "Begin in a plan k position.  The aim is to keep your body rigid, but then come up onto your right hand straightening your arm, then your left, then down on the right and down on the left.Repeat.";
		exerciseTextArray[3] = "In the push up position, begin to bring your knee into your chin/chest.  Repeat the other side and then build up momentum.";
		exerciseTextArray[4] = "From the push up position, pres up an then controlling the balance, twist sideways so your feet are stacked and your arms in a T position.  Hold this then return, push up and perform the other side.";
		exerciseTextArray[5] = "Perform a standard jumping jack, landing softly and with knees slightly bent.";
		exerciseTextArray[6] = "On your back, lift your legs to vertical.  Arms at side or holding a stationary object for support over your head.  Lift your hips, stopping your legs from swaying.Lower to start and repeat.";
		exerciseTextArray[7] = "Similar to sky pointers except this time have the soles of your feet together, knees split.  Keep this position as you then raise your legs, lift your hips, then return to start and repeat.";

		exerciseNameArray[0] = "Close Grip Push Up";
		exerciseNameArray[1] = "BW Squat Jumps";
		exerciseNameArray[2] = "Plank Climber";
		exerciseNameArray[3] = "Nose 2 Knee Breakers";
		exerciseNameArray[4] = "T-Bar Push Up";
		exerciseNameArray[5] = "Jumping Jacks";
		exerciseNameArray[6] = "Sky Pointers";
		exerciseNameArray[7] = "Split Frogs";

		firstPictureArray[0] = R.drawable.w3_exercise01_01;
		firstPictureArray[1] = R.drawable.w3_exercise02_01;
		firstPictureArray[2] = R.drawable.w3_exercise03_01;
		firstPictureArray[3] = R.drawable.w3_exercise04_01;
		firstPictureArray[4] = R.drawable.w3_exercise05_01;
		firstPictureArray[5] = R.drawable.w3_exercise06_01;
		firstPictureArray[6] = R.drawable.w3_exercise07_01;
		firstPictureArray[7] = R.drawable.w3_exercise08_01;

		secondPictureArray[0] = R.drawable.w3_exercise01_02;
		secondPictureArray[1] = R.drawable.w3_exercise02_02;
		secondPictureArray[2] = R.drawable.w3_exercise03_02;
		secondPictureArray[3] = R.drawable.w3_exercise04_02;
		secondPictureArray[4] = R.drawable.w3_exercise05_02;
		secondPictureArray[5] = R.drawable.w3_exercise06_02;
		secondPictureArray[6] = R.drawable.w3_exercise07_02;
		secondPictureArray[7] = R.drawable.w3_exercise08_02;

		thirdPictureArray[0] = 0;
		thirdPictureArray[1] = 0;
		thirdPictureArray[2] = R.drawable.w3_exercise03_03;
		thirdPictureArray[3] = 0;
		thirdPictureArray[4] = R.drawable.w3_exercise05_03;
		thirdPictureArray[5] = 0;
		thirdPictureArray[6] = 0;
		thirdPictureArray[7] = 0;

		imageCount[0] = 2;
		imageCount[1] = 2;
		imageCount[2] = 3;
		imageCount[3] = 2;
		imageCount[4] = 3;
		imageCount[5] = 2;
		imageCount[6] = 2;
		imageCount[7] = 2;

	}

	public void workoutDExercise() {
		titleView.setBackgroundResource(R.drawable.title04);
		exerciseTextArray = new String[8];
		exerciseNameArray = new String[8];
		firstPictureArray = new int[8];
		secondPictureArray = new int[8];
		thirdPictureArray = new int[8];
		imageCount = new int[8];

		exerciseTextArray[0] = "Holding your DBs at 90 degrees, stabilize, bend your knees a fraction to put tension on the core, and then press up both weights and return.Repeat.";
		exerciseTextArray[1] = "Using a pull up machine or any sturdy pull up equipment, grip with a wide overhand grip and pull yourself up, tensing your core, so your chest nearly touches the bar.  Pause, return slowly and repeat.";
		exerciseTextArray[2] = "Holding dumbbells in a push up position, tense your core and body, as you twist round bringing the DB upward to a side stationary T-bar hold.  Stack your feet if you can.  Return under control and repeat the other side.";
		exerciseTextArray[3] = "Holding two dumbbells at your shoulders, elbows toward the front, squat downward so your thighs are parallel and then thrust upward, as you simultaneously press the DBs overhead.  Return under control and repeat.";
		exerciseTextArray[4] = "With DBs in your hands, get into a push up position, but have one DB at your side in a close grip pus h up position, the other out in front.  Perform a push up then switch hand positions for 10 on the other side.";
		exerciseTextArray[5] = "Balancing on your butt, with your feet raised slightly, hold a DB and twist to either side, holding the weight.";
		exerciseTextArray[6] = "Holding your weights, get into a fighter stance, one foot in front of the other for stabilization, and begin performing upper cuts, in a slow controlled manner until you feel comfortable.  Keep your core tensed and knees bent, bouncing slightly on knees as you perform the move.";
		exerciseTextArray[7] = "In the plan k position, hold the move for 5 seconds, rest for 5 seconds.(10 times)";

		exerciseNameArray[0] = "Db Overhead Press";
		exerciseNameArray[1] = "Pull Ups";
		exerciseNameArray[2] = "Db T-Bar Twist";
		exerciseNameArray[3] = "Thrusters";
		exerciseNameArray[4] = "DB Zig Zag Push Up";
		exerciseNameArray[5] = "DB Balance Twists";
		exerciseNameArray[6] = "DB UpperCuts";
		exerciseNameArray[7] = "PAUSE Plank";

		firstPictureArray[0] = R.drawable.w4_exercise01_01;
		firstPictureArray[1] = R.drawable.w4_exercise02_01;
		firstPictureArray[2] = R.drawable.w4_exercise03_01;
		firstPictureArray[3] = R.drawable.w4_exercise04_01;
		firstPictureArray[4] = R.drawable.w4_exercise05_01;
		firstPictureArray[5] = R.drawable.w4_exercise06_01;
		firstPictureArray[6] = R.drawable.w4_exercise07_01;
		firstPictureArray[7] = R.drawable.w4_exercise08_01;

		secondPictureArray[0] = R.drawable.w4_exercise01_02;
		secondPictureArray[1] = R.drawable.w4_exercise02_02;
		secondPictureArray[2] = R.drawable.w4_exercise03_02;
		secondPictureArray[3] = R.drawable.w4_exercise04_02;
		secondPictureArray[4] = R.drawable.w4_exercise05_02;
		secondPictureArray[5] = R.drawable.w4_exercise06_02;
		secondPictureArray[6] = R.drawable.w4_exercise07_02;
		secondPictureArray[7] = R.drawable.w4_exercise08_02;

		thirdPictureArray[0] = 0;
		thirdPictureArray[1] = 0;
		thirdPictureArray[2] = 0;
		thirdPictureArray[3] = 0;
		thirdPictureArray[4] = 0;
		thirdPictureArray[5] = 0;
		thirdPictureArray[6] = 0;
		thirdPictureArray[7] = 0;

		imageCount[0] = 2;
		imageCount[1] = 2;
		imageCount[2] = 2;
		imageCount[3] = 2;
		imageCount[4] = 2;
		imageCount[5] = 2;
		imageCount[6] = 2;
		imageCount[7] = 2;

	}

	public void workoutEExercise() {
		titleView.setBackgroundResource(R.drawable.title05);
		exerciseTextArray = new String[8];
		exerciseNameArray = new String[8];
		firstPictureArray = new int[8];
		secondPictureArray = new int[8];
		thirdPictureArray = new int[8];
		imageCount = new int[8];

		exerciseTextArray[0] = "Holding DBs in a hammer grip, bend your knees slightly, tense your core and curl the weight up to your shoulder and return under control.Repeat.";
		exerciseTextArray[1] = "With heavier DBs at your side, bend your knees slightly and focus stability onto your core.  Now shrug DIAGONALLY back and upwards, pulling your shoulder blades together.  Don�t shrug upwards.  Repeat.";
		exerciseTextArray[2] = "Holding DBs at your side, lunge backward on your left leg, shoulders back, chin up ward to a point where your knee is at 90 degrees, pause then return upward and repeat on other leg.";
		exerciseTextArray[3] = "Bend at the waist, holding 2 DBs.  Keep your back flat and bend your knees a fraction.  Begin rowing the weight to your waist, pause., return and repeat.";
		exerciseTextArray[4] = "Laying on the floor or a bench, press the DBs upward, then slowly back to the start position.";
		exerciseTextArray[5] = "The traditional military exercise:  from standing drop down with hands on the floor and �jump� your feet backward to a push up position, jump them back to a tucked position then explode upwards, jumping off the ground, land softly and immediately repeat.";
		exerciseTextArray[6] = "In the push up position, begin to bring your knee into your chin/chest.  Repeat the other side and then build up momentum.";
		exerciseTextArray[7] = "On your back, lift your foot upward and raise from the wait lifting shoulders off the floor your butt as the pivot. Touch left foot with left hand, return and repeat on other side.";

		exerciseNameArray[0] = "Hammer Curls";
		exerciseNameArray[1] = "DB Diagonal Shrugs";
		exerciseNameArray[2] = "Reverse Alt. DB Lunges";
		exerciseNameArray[3] = "Double DB Bent Row";
		exerciseNameArray[4] = "DB Floor [or Bench] Press";
		exerciseNameArray[5] = "Burpees";
		exerciseNameArray[6] = "Nose 2 Knee Breakers";
		exerciseNameArray[7] = "Half & Half";

		firstPictureArray[0] = R.drawable.w5_exercise01_01;
		firstPictureArray[1] = R.drawable.w5_exercise02_01;
		firstPictureArray[2] = R.drawable.w5_exercise03_01;
		firstPictureArray[3] = R.drawable.w5_exercise04_01;
		firstPictureArray[4] = R.drawable.w5_exercise05_01;
		firstPictureArray[5] = R.drawable.w5_exercise06_01;
		firstPictureArray[6] = R.drawable.w5_exercise07_01;
		firstPictureArray[7] = R.drawable.w5_exercise08_01;

		secondPictureArray[0] = R.drawable.w5_exercise01_02;
		secondPictureArray[1] = R.drawable.w5_exercise02_02;
		secondPictureArray[2] = R.drawable.w5_exercise03_02;
		secondPictureArray[3] = R.drawable.w5_exercise04_02;
		secondPictureArray[4] = R.drawable.w5_exercise05_02;
		secondPictureArray[5] = R.drawable.w5_exercise06_02;
		secondPictureArray[6] = R.drawable.w5_exercise07_02;
		secondPictureArray[7] = R.drawable.w5_exercise08_02;

		thirdPictureArray[0] = 0;
		thirdPictureArray[1] = 0;
		thirdPictureArray[2] = 0;
		thirdPictureArray[3] = 0;
		thirdPictureArray[4] = 0;
		thirdPictureArray[5] = R.drawable.w5_exercise06_03;
		thirdPictureArray[6] = 0;
		thirdPictureArray[7] = 0;

		imageCount[0] = 2;
		imageCount[1] = 2;
		imageCount[2] = 2;
		imageCount[3] = 2;
		imageCount[4] = 2;
		imageCount[5] = 4;
		imageCount[6] = 2;
		imageCount[7] = 2;

	}

}
